import { LoginComponent} from "./components/login/login.component";
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
//import { AuthGuard } from './shared/guard/auth.guard';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes =[
    {
        path: '',
        loadChildren: './components/coverpage/coverpage.module#CoverpageModule'
    },
    {
        path: 'admin',
        loadChildren: './components/admin/admin.module#AdminModule',
        //canActivate: [AuthGuard]
    },
    {
        path: 'login',
        loadChildren: './components/login/login.module#LoginModule'
    },
    {
        path: 'agent',
        loadChildren: './components/agent/agent.module#AgentModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule{}