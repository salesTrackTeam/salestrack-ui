export interface Agent{
    agentId: number,
    userId: number,
    companyid:number,
    agentFirstName: string,
    agentLastName: string,
    agentEmail: string,
    agentPhone: string,
    message: string,
    createDate: string,
    password:string,
    profilepic:string,
    userName:string
}