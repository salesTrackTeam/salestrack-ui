export interface User {
    userId:string,
    companyId:string,
    companyLoginName:string,
    companyName:string,
    userFirstName:string,
    userLastName:string,
    userRole:string,
    userType:string,
    profilePic:string,
    email:string,
    phone:string,
    address:string,
    isActive:string,
	deactivationDate:string,
    loginFlag:string,
    agentId:string
}