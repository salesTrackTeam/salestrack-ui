export interface Dealer{
    agentId: number,
    dealerId: number,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    message: string,
    registerDate: string,
    latitude: string,
    longitude: string,
    agentFirstName: string,
    agentLastName: string
}