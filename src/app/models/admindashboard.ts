export interface AdminDashboard{
    monthlyIncome: string,
    orders: string,
    visits: string,
    userActivity: string,
    recentTrade:RecentTrade[],
    recentAgentActivity:RecentAgentActivity[],
    toDoList:ToDoList[],
    compositeTransaction:CompositeTransaction[]
}

interface RecentTrade{
    dealerName:string,
    agentName:string,
    date:string,
    amount:string
}
interface RecentAgentActivity{
    status:string,
    date:string,
    user:String,
    value:string,
    noOfDealerAdded:string;
}
interface ToDoList{
    toDo:string
}
interface CompositeTransaction{
    transaction:string,
    date:string,
    amount:string,

}