import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { User } from '../models/user';
import { Observable } from 'rxjs';


@Injectable()
export class AuthenticateService {

  constructor(private http: HttpClient) {
}

    login(username,password,company):Observable<User>{
        return this.http.get<User>("/user/authenticate/"+company+'/'+username+'/'+password);
    }  

    enquiry(enquirydata){
        console.log('enquirydata in agent.service.ts---'+enquirydata.name);
        return this.http.post("/enquiry", JSON.stringify(enquirydata), {headers: {'Content-Type': 'application/json'}})
        .pipe(map((res) => {
            var responcseData = JSON.stringify(res);
            console.log('enquiry details in agent.service.ts---'+JSON.stringify(res));
            return responcseData;
        }));
    }  
    signup(signupData) {
        return this.http.post("/user/signup", JSON.stringify(signupData), {headers: {'Content-Type': 'application/json'}})
        .pipe(map((res) => {
            var responseData = JSON.stringify(res);
            console.log('addCustomer in admin.service.ts---'+JSON.stringify(res));
            return responseData;
        }));
    }
}





 

