import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Agent } from "../../models/agent";
import { Dealer } from "../../models/dealer";
import { Observable } from 'rxjs';
import { AdminDashboard } from '../../models/admindashboard';

@Injectable()
export class AdminService {

  constructor(private http: HttpClient) {
}


getAdminDashboardData(userId,companyId):Observable<AdminDashboard>{
    return this.http.get<AdminDashboard>('/admin/getAdminDashboardData/'+userId+'/'+companyId);
}  

addAgent(agentData) {
        return this.http.post("/admin/addAgent", JSON.stringify(agentData), {headers: {'Content-Type': 'application/json'}})
        .pipe(map((res) => {
            var responcseData = JSON.stringify(res);
            console.log('addAgent in admin.service.ts---'+JSON.stringify(res));
            return responcseData;
        }));
    }
    
modifyAgent(agentData) {
        return this.http.post("/admin/modifyAgent", JSON.stringify(agentData), {headers: {'Content-Type': 'application/json'}})
        .pipe(map((res) => {
            var responcseData = JSON.stringify(res);
            console.log('addAgent in admin.service.ts---'+JSON.stringify(res));
            return responcseData;
        }));
    }
getAgentProfileData(userId,companyId,agentId){
    return this.http.get<Agent>('/admin/getAgentProfileData/'+userId+'/'+companyId+'/'+agentId);
}  

getDealerDetails(userId,companyId,dealerId,fromWhere){
    return this.http.get<Dealer>('/admin/getDealerDetails/'+userId+'/'+companyId+'/'+dealerId+'/'+fromWhere);
}

getAgents(userId,companyId):Observable<Agent[]>{
        return this.http.get<Agent[]>('/admin/getAgentByAdminId/'+userId+'/'+companyId);
    }    

getDealerList (userId,companyId,agentId):Observable<Dealer[]>{
        return this.http.get<Dealer[]>('/admin/getDealerByAdminId/'+userId+'/'+companyId+'/'+agentId);
    }  
}


