import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import {Dealer} from "../../models/dealer";
import { Observable } from 'rxjs';

@Injectable()
export class AgentService {

  constructor(private http: HttpClient) {
}

addLeadData(leadData) {
        return this.http.post("/agent/saveDealer", JSON.stringify(leadData), {headers: {'Content-Type': 'application/json'}})
        .pipe(map((res) => {
            var responcseData = JSON.stringify(res);
            console.log('addLeadData in agent.service.ts---'+JSON.stringify(res));
            return responcseData;
        }));
    }


getDealers(agentId):Observable<Dealer[]>{
        return this.http.get<Dealer[]>('/agent/getDealerByAgentId/'+agentId);
    }    
   
}





 

