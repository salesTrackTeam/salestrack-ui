import { User } from "../models/user";

export class UserService{

    public setUserData(userData:User){
        window.localStorage.setItem("userId",userData.userId);
        window.localStorage.setItem("companyId",userData.companyId);
        window.localStorage.setItem("companyName",userData.companyName);
        window.localStorage.setItem("companyLoginName",userData.companyLoginName);
        window.localStorage.setItem("userFirstName",userData.userFirstName);
        window.localStorage.setItem("userLastName",userData.userLastName);
        window.localStorage.setItem("userRole",userData.userRole);
        window.localStorage.setItem("userType",userData.userType);
        window.localStorage.setItem("profilePic",userData.profilePic);
        window.localStorage.setItem("email",userData.email);
        window.localStorage.setItem("phone",userData.phone);
        window.localStorage.setItem("address",userData.address);
        window.localStorage.setItem("isActive",userData.isActive);
        window.localStorage.setItem("deactivationDate:",userData.deactivationDate);
        window.localStorage.setItem("loginFlag",userData.loginFlag);
        window.localStorage.setItem("agentId",userData.agentId);
    }

}


