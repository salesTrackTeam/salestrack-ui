import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {InfopageComponent} from "./infopage.component";
import {infopageRoutes} from "./infopage.routes";
import {AgentService} from '../../../service/agentService/agent.service';
import { AdminService} from '../../../service/adminService/admin.service';
import { DataTablesModule } from 'angular-datatables';
import {DealerdetailsComponent} from '../../agent/dealerdetails/dealerdetails.component';


@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      DataTablesModule,
      RouterModule.forChild(infopageRoutes)
  ],
  declarations: [
    InfopageComponent
  ],
  providers:[
    AgentService,
    AdminService,
    DealerdetailsComponent
  ]
})
export class InfopageModule { }
