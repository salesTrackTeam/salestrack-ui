import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {AgentService} from '../../../service/agentService/agent.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import {DealerdetailsComponent} from '../dealerdetails/dealerdetails.component';

@Component({
  selector: 'app-leadinfopage',
  templateUrl: './infopage.component.html',
  styleUrls: ['./infopage.component.css']
})
export class InfopageComponent implements OnDestroy, OnInit {
  
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  public header:string;
  public dealers = [];
  constructor(
    private router: Router,
    private agentService:AgentService,
    private DealerdetailsComponent:DealerdetailsComponent
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
      scrollX: true,
      buttons: [
        {extend: 'pageLength',className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       }},
        { extend: 'excel', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       }},
        { extend: 'csv', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } },
        { extend: 'pdf', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } },
        { extend: 'print', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } }
      ]
    };

    this.agentService.getDealers(window.localStorage.getItem("agentId"))
      .subscribe(dealerList => {
        this.dealers = dealerList
        this.dtTrigger.next();
      });
    this.header = "My Dealers";
  }

  openprofile(dealerid): void {
    this.router.navigate(['./agent/dealerDetails']);
    console.log("1111111111111111");
    this.DealerdetailsComponent.loadProfile(dealerid,'agent');
    dealerid = parseInt(dealerid);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  
  addbtnClick= function () {
    this.router.navigate(['./agent/addLead']);
  };

}
