import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../../models/user";

@Component({
    selector: 'agent-navigation',
    templateUrl: './navigation.component.html'
})
export class Navigation implements OnInit {
    @Input() loginInfo:User;
    constructor( private router: Router) { }

    ngOnInit() { }
    activeRoute(routename: string): boolean{
        return this.router.url.indexOf(routename) > -1;
    }
}