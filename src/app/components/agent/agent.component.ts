import { Component } from '@angular/core';
import { User } from "../../models/user";

@Component({
  selector: 'app-agentadmin',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.css']
})
export class AgentComponent {

  loginInfo ={
    first_name: window.localStorage.getItem("userFirstName"),
    last_name:window.localStorage.getItem("userLastName"),
    avatar:window.localStorage.getItem("profilePic"),
    title:window.localStorage.getItem("company")
};

}
