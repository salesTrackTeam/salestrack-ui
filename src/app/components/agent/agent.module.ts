import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {Topnavbar} from "./topnavbar/topnavbar.component";
import {Navigation} from "./navigation/navigation.component";
import {AgentComponent } from './agent.component';
import {AgentRoutingModule } from './agent-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AgentRoutingModule,
    FormsModule,
    HttpModule
  ],declarations: [
    AgentComponent,
    Navigation,
    Topnavbar
  ]
})
export class AgentModule { }
