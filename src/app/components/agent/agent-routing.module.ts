import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AgentComponent } from './agent.component';

const routes: Routes = [
  {
      path: '',
      component: AgentComponent,
      children: [
          {
              path: '',
              redirectTo: 'info/dealerList'
          },
          {
              path: 'info/:infoPageId',
              loadChildren: './info/infopage.module#InfopageModule'
          },
          {
               path: 'dealerDetails',  
               loadChildren: './dealerdetails/dealerdetails.module#DealerdetailsModule'
          },
          {
              path: 'addLead',
              loadChildren: './addleadform/addleadform.module#AddLeadModule'
          }
          
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule { }
