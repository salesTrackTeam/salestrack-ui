import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { DealerdetailsComponent} from '../dealerdetails/dealerdetails.component';
import {dealerdetailsRoutes} from '../dealerdetails/dealerdetails.routes';
import {AdminService} from '../../../service/adminService/admin.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(dealerdetailsRoutes)
  ],
  declarations: [
    DealerdetailsComponent
  ],
  providers:[
    AdminService
  ]
})
export class DealerdetailsModule { }
