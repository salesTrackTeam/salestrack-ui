import {DealerdetailsComponent} from "./dealerdetails.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const dealerdetailsRoutes=[
    {
        path:'',
        component:DealerdetailsComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(dealerdetailsRoutes)],
    exports: [RouterModule]
})
export class DealerdetailsRoutingModule {}