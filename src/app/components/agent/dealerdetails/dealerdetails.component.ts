import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Dealer} from '../../../models/dealer';
import { AdminService} from '../../../service/adminService/admin.service';


@Component({
  selector: 'app-dealerdetails',
  templateUrl: './dealerdetails.component.html',
  styleUrls: ['./dealerdetails.component.css']
})
export class DealerdetailsComponent implements OnInit {

  public dealerDetails:Dealer;
  public static dealerId:string;
  public static username:string;
  public static fromWhere:string;
  url = 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';

  constructor(
    private router: Router,
    private adminService: AdminService
  ) { }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
        console.log("url:::"+this.url);
      }
    }
  }

  ngOnInit() {
  }

  loadProfile(dealerId,fromWhere){
    DealerdetailsComponent.dealerId = dealerId;
    this.adminService.getDealerDetails(window.localStorage.getItem("userId"),window.localStorage.getItem("companyId"),dealerId,fromWhere)
    .subscribe(
      response => {
        delearDeatails => this.dealerDetails = delearDeatails;
        //(<HTMLInputElement>document.getElementById("username")).textContent = "Dealer Details";
        DealerdetailsComponent.fromWhere = fromWhere;
        (<HTMLInputElement>document.getElementById("firstname")).value = response.firstName;
        (<HTMLInputElement>document.getElementById("lastname")).value = response.lastName;
        (<HTMLInputElement>document.getElementById("email")).value = response.email;
        (<HTMLInputElement>document.getElementById("phone")).value = response.phone;
        (<HTMLInputElement>document.getElementById("doe")).value = response.registerDate;
        (<HTMLInputElement>document.getElementById("addedBy")).value = response.agentFirstName+" "+response.agentLastName;
        (<HTMLInputElement>document.getElementById("message")).value = response.message;
        },
      err => {
      console.error('Something went wrong in Dealer Details ', err);
      });
  }

  updateProfile(event) {
    event.preventDefault();
    const target = event.target;
    var data = {
      'userId' : window.localStorage.getItem("userId"),
      'companyId':window.localStorage.getItem("companyId"),
      'dealerId':DealerdetailsComponent.dealerId,
      'userName':DealerdetailsComponent.username,
      'agentFirstName' : target.querySelector("#firstname").value,
      'agentLastName' : target.querySelector("#lastname").value,
      'agentEmail' : target.querySelector("#email").value,
      'agentPhone' : target.querySelector("#phone").value,
      'message' : target.querySelector("#message").value,
      'password' : target.querySelector("#doe").value
    };

    this.adminService.modifyAgent(data)
    .subscribe(
      response => {
        console.log("addAgentFormComponent----"+response);
        var responseData = JSON.parse(response);
        console.log("addAgentFormComponent--111--"+responseData.result);
          if('success'==responseData.result){
            this.router.navigate(['/admin/agent']);
            //this.responseMessage = "Generated Username = "+responseData.username+" and Password = "+responseData.password+".";
            //this.showSuccessMessage(event);
          }else if('failed'==responseData.result){
            //this.showErrorMessage(event);
        }

        },
      err => {
      console.error('Something went wrong in Adding Dealer', err);
      //this.showErrorMessage(event);
      });
  };

  backbtnClick= function () {
    if('agent'==DealerdetailsComponent.fromWhere){
      this.router.navigate(['./agent/info/dealerList']);
    }else if('admin'==DealerdetailsComponent.fromWhere){
      this.router.navigate(['./admin/dealer']);
    }
  };
}
