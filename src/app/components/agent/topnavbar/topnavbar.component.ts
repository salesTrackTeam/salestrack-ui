import { Component } from '@angular/core';
import { Router } from '@angular/router';

import {smoothlyMenu} from "../app.helpers";

@Component({
    selector: 'agent-topnavbar',
    templateUrl: 'topnavbar.component.html'
})
export class Topnavbar {

    constructor(private router: Router) {}

    ngOnInit() {

    }
    toggleNavigation(): void {
        jQuery("body").toggleClass("mini-navbar");
        smoothlyMenu();
    }
    logout() {
        localStorage.clear();
        this.router.navigate(['./login']);
        // location.href='http://to_login_page';
    }
}