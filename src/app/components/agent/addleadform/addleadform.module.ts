import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {AddleadformComponent} from "./addleadform.component";
import {addleadRoutes} from "./addleadform.routes";
import { AgentService } from '../../../service/agentService/agent.service';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      RouterModule.forChild(addleadRoutes)
  ],
  declarations: [
    AddleadformComponent
  ],
  providers: [
    AgentService
]
})
export class AddLeadModule { }
