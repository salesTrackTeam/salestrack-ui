import {AddleadformComponent} from "./addleadform.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const addleadRoutes=[
    {
        path:'',
        component:AddleadformComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(addleadRoutes)],
    exports: [RouterModule]
})
export class AddLeadFormRoutingModule {}