import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgentService } from '../../../service/agentService/agent.service';
import { stringify } from '@angular/core/src/render3/util';

@Component({
  selector: 'app-addleadform',
  templateUrl: './addleadform.component.html',
  //styleUrls: ['./adddealerform.component.css']
})
export class AddleadformComponent implements OnInit {
  
  responseMessage:string;

  constructor(
    private router: Router,
    private agentService: AgentService
  ) { }

  data = {
    'agentId' : "",
    'firstName' : "",
    'lastName' : "",
    'email' : "",
    'phone' : "",
    'message' : "",
    'latitude' : 0.00,
    'longitude' :0.00
  };


  addWithLocation(position) {
    
  }


  addLead(event) {
    event.preventDefault();
    const target = event.target;

    this.data.agentId = window.localStorage.getItem("agentId");
    this.data.firstName = target.querySelector("#firstname").value;
    this.data.lastName = target.querySelector("#lastname").value;
    this.data.email = target.querySelector("#email").value;
    this.data.phone = target.querySelector("#phone").value;
    this.data.message = target.querySelector("#message").value;
  

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position)=>{
        this.data.latitude = position.coords.latitude;
        this.data.longitude = position.coords.longitude; 
    
        this.agentService.addLeadData(this.data)
        .subscribe(
          response => {
                var responseData = JSON.parse(response);
                if('success'==responseData.result){
                  this.responseMessage = "Lead Added Sussessfully";
                  this.showSuccessMessage(event);
                }else if('failed'==responseData.result){
                  this.showErrorMessage(event);
            }
          },
          err => {
          console.error('Something went wrong in Adding Lead By Agent ', err);
          this.showErrorMessage(event);
          });
      });
    } else {
      console.log("Geolocation is not supported by this browser.");
    }

  };
  
  showSuccessMessage(event){
    console.log('showSuccessMessage---');
    const target = event.target;
    target.querySelector("#response_message").style.display = "block";
    target.querySelector("#main_form").style.display = "none";
    target.querySelector("#error_message").style.display = "none";
    }
  showErrorMessage(event){
    const target = event.target;
    target.querySelector("#response_message").style.display = "none";
    target.querySelector("#main_form").style.display = "none";
    target.querySelector("#error_message").style.display = "block";
    }
  addMore(event){
    const target = event.target;
    (<HTMLInputElement>document.getElementById("firstname")).value = "";
    (<HTMLInputElement>document.getElementById("lastname")).value = "";
    (<HTMLInputElement>document.getElementById("email")).value = "";
    (<HTMLInputElement>document.getElementById("phone")).value = "";
    (<HTMLInputElement>document.getElementById("message")).value = "";
    
    window.document.getElementById("response_message").style.display = "none";
    window.document.getElementById("main_form").style.display = "block";
    window.document.getElementById("error_message").style.display = "none";
    }
    backbtnClick= function () {
      this.router.navigate(['./agent/info/dealerList']);
    };
  ngOnInit() {}
}
