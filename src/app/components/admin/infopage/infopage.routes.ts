import {InfopageComponent} from "./infopage.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const infopageRoutes=[
    {
        path:'',
        component:InfopageComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(infopageRoutes)],
    exports: [RouterModule]
})
export class InfopageRoutingModule {}