import { Component, OnInit ,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { AdminService} from '../../../service/adminService/admin.service';
import { MatDialog} from '@angular/material';
import {ManageagentprofileComponent} from '../manageagentprofile/manageagentprofile.component';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-infopage',
  templateUrl: './agent-info.component.html',
  styleUrls: ['./infopage.component.css']
})
export class InfopageComponent implements OnDestroy, OnInit {
  //dtOptions: DataTables.Settings = {};
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  public header:string;
  public agents = [];
  
  constructor(
    private router: Router,
    private adminService: AdminService,
    public dialog: MatDialog,
    public ManageagentprofileComponent:ManageagentprofileComponent
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
      scrollX: true,
      buttons: [
        {extend: 'pageLength',className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       }},
        { extend: 'excel', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       }},
        { extend: 'csv', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } },
        { extend: 'pdf', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } },
        { extend: 'print', className: 'btn btn-primary',init: function( api, node, config) {
          $(node).removeClass('dt-button')
       } }
      ]
    };

      this.header = "Agent List";
      this.adminService.getAgents(window.localStorage.getItem("userId"),window.localStorage.getItem("companyId"))
      .subscribe(agentList => {

        this.agents = agentList;
        this.dtTrigger.next();
      });
      
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  openprofile(agentid): void {

    this.router.navigate(['./admin/manageagentprofile']);
    this.ManageagentprofileComponent.loadProfile(agentid);
    agentid = parseInt(agentid);

    /*
    const dialogRef = this.dialog.open(AgentprofileComponent, {
      closeOnNavigation:true,
      width:'90%',
      height:'90%',
      data: {agentid: agentid}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
    */
  }

  addAgent= function () {
    this.router.navigate(['./admin/addagent']);
  };

  backbtnClick= function () {
    this.router.navigate(['./admin/home']);
  };
}

//Dealers---------------------------------------------------------------------------------------------------
