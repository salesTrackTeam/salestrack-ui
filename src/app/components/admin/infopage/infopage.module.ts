import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {InfopageComponent} from "./infopage.component";
import {infopageRoutes} from "./infopage.routes";
import {AdminService} from '../../../service/adminService/admin.service';
import {MatDialog} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {AgentprofileComponent} from '../agentprofile/agentprofile.component';
import {ManageagentprofileComponent} from '../manageagentprofile/manageagentprofile.component';
import { DataTablesModule } from 'angular-datatables';
 
@NgModule({
  imports: [
      CommonModule,
      DataTablesModule,
      FormsModule,
      MatDialogModule,
      RouterModule.forChild(infopageRoutes)
  ],
  declarations: [
    InfopageComponent,
    AgentprofileComponent
  ],
  providers:[
    AdminService,
    MatDialog,
    ManageagentprofileComponent
  ],
  entryComponents: [
    InfopageComponent,
    AgentprofileComponent
  ]
})
export class InfopageModule {


 }
