import {OnInit, Component,Input} from "@angular/core";
import { AdminService} from '../../../service/adminService/admin.service';
import {User} from "../../../models/user";

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
    
    public adminDashboardData;
    public userInfoData: User;
    public data:String;
    constructor(private adminService: AdminService){
    }
    
    ngOnInit() {
        this.adminService.getAdminDashboardData(window.localStorage.getItem("userId"),window.localStorage.getItem("companyId"))
        .subscribe(responseData => this.adminDashboardData = responseData);
        
        this.data = window.localStorage.getItem("userInfoData");
    }

}