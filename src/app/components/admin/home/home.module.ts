import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {AdminService} from '../../../service/adminService/admin.service';
import { UserService } from '../../../service/user.service';

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule
    ],
    declarations: [HomeComponent
    ],
    providers:[
        AdminService,
        UserService
    ]
})
export class HomeModule {}
