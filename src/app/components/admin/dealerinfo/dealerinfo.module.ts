import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {DealerinfoComponent,DialogOverviewExampleDialog} from "./dealerinfo.component";
import {AdminService} from '../../../service/adminService/admin.service';
import {OthersComponent} from '../others/others.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {AgmCoreModule } from '@agm/core';
import {dealerinfoRoutes} from "./dealerinfo.routes";
import { DataTablesModule } from 'angular-datatables';
import {DealerdetailsComponent} from '../../agent/dealerdetails/dealerdetails.component';


@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      DataTablesModule,
      RouterModule.forChild(dealerinfoRoutes),
      MatDialogModule,
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyDLgqsTQPQLLe2wBNMefM7FhzEnYxwQAYc'
      })      
  ],
  declarations: [
    DealerinfoComponent,
    DialogOverviewExampleDialog    
  ],
  providers:[
    AdminService,
    MatDialog,
    DealerdetailsComponent
  ],
  entryComponents: [
    DialogOverviewExampleDialog
  ]
})
export class DealerinfoModule { }
