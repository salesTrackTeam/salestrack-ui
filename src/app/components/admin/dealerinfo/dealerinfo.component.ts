import { Component, OnInit ,Inject,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { AdminService} from '../../../service/adminService/admin.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MouseEvent } from '@agm/core';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import {DealerdetailsComponent} from '../../agent/dealerdetails/dealerdetails.component';

export interface DialogData {
	latitude: number;
	longitude: number;
}
interface marker {
	latitude: number;
	longitude: number;
	label?: string;
	draggable: boolean;
}


@Component({
  selector: 'app-dealerinfo',
  templateUrl: './dealerinfo.component.html',
  styleUrls: ['./dealerinfo.component.css']
})
export class DealerinfoComponent implements OnDestroy, OnInit {
    dtOptions: any = {};
    dtTrigger: Subject<any> = new Subject();

    public header:string;
    public dealers = [];
  
    latitude: number;
    longitude: number;
    markers: marker[];
  
    constructor(
      private router: Router,
      private adminService: AdminService,
      public dialog: MatDialog,
      public DealerdetailsComponent:DealerdetailsComponent
    ) { }
  
    ngOnInit() {
      
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        dom: 'Bfrtip',
        scrollX: true,
        buttons: [
          {extend: 'pageLength',className: 'btn btn-primary',init: function( api, node, config) {
            $(node).removeClass('dt-button')
         }},
          { extend: 'excel', className: 'btn btn-primary',init: function( api, node, config) {
            $(node).removeClass('dt-button')
         }},
          { extend: 'csv', className: 'btn btn-primary',init: function( api, node, config) {
            $(node).removeClass('dt-button')
         } },
          { extend: 'pdf', className: 'btn btn-primary',init: function( api, node, config) {
            $(node).removeClass('dt-button')
         } },
          { extend: 'print', className: 'btn btn-primary',init: function( api, node, config) {
            $(node).removeClass('dt-button')
         } }
        ]
      };

        this.header = "Dealer List";
        this.adminService.getDealerList(window.localStorage.getItem("userId"),window.localStorage.getItem("companyId"),-1)
        .subscribe(dealerList => {
          this.dealers = dealerList
          this.dtTrigger.next();
        });
    }
    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }
    
    openMap(lat,lng): void {
      lat = Number(lat).valueOf();// parseInt(lat);
      lng = Number(lng).valueOf();// parseInt(lng);
      const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
        closeOnNavigation:true,
        width:'80%',
        height:'80%',
        data: {latitude: lat, longitude:lng }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.latitude = result;
      });
    }
  
    openprofile(dealerid): void {

      this.router.navigate(['./admin/dealerDetails']);
      this.DealerdetailsComponent.loadProfile(dealerid,'admin');
      dealerid = parseInt(dealerid);
    }

    backbtnClick= function () {
      this.router.navigate(['./admin/home']);
    };
  }
  //---------------------------------------------------------------------------------------------------
  @Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog-overview-example-dialog.html',
    styleUrls: ['./dealerinfo.component.css']
  })
  export class DialogOverviewExampleDialog {
  
    constructor(
      public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        /*this.markers.push({
          latitude : data.latitude,
          longitude : data.longitude,
          label: '',
          draggable: false
        });*/
      }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
    zoom: number = 15;
    
    // initial center position for the map
    latitude: number ;
    longitude: number ;
  
    markers: marker[];
    clickedMarker(label: string, index: number) {
      console.log(`clicked the marker: ${label || index}`)
    }
    
    mapClicked($event: MouseEvent) {
      //do nothing
      // this.markers.push({
      //   latitude: $event.coords.latitude,
      //   longitude: $event.coords.longitude,
      //   draggable: true
      // });
    }
    
    markerDragEnd(m: marker, $event: MouseEvent) {
      console.log('dragEnd', m, $event);
    }
    
    
  
  }
