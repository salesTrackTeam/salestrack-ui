import {DealerinfoComponent} from "./dealerinfo.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const dealerinfoRoutes=[
    {
        path:'',
        component:DealerinfoComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(dealerinfoRoutes)],
    exports: [RouterModule]
})
export class DealerinfoRoutingModule {}