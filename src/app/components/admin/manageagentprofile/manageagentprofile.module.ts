import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { ManageagentprofileComponent} from './manageagentprofile.component';
import {manageagentprofileRoutes} from './manageagentprofile.routes';
import {AdminService} from '../../../service/adminService/admin.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(manageagentprofileRoutes)
  ],
  declarations: [
    ManageagentprofileComponent
  ],
  providers:[
    AdminService
  ]
})
export class ManageagentprofileModule { }
