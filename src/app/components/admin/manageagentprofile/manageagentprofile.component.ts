import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Agent} from '../../../models/agent';
import { AdminService} from '../../../service/adminService/admin.service';


var angular:any;
@Component({
  selector: 'app-manageagentprofile',
  templateUrl: './manageagentprofile.component.html',
  styleUrls: ['./manageagentprofile.component.css']
})
export class ManageagentprofileComponent implements OnInit {

  public agentprofile:Agent;
  public static agentId:string;
  public static username:string;
  url = 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';

  constructor(
    private router: Router,
    private adminService: AdminService
  ) { }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
        console.log("url:::"+this.url);
      }
    }
  }

  ngOnInit() {
  }

  loadProfile(agentid){
    ManageagentprofileComponent.agentId = agentid;
    this.adminService.getAgentProfileData(window.localStorage.getItem("userId"),window.localStorage.getItem("companyId"),agentid)
    .subscribe(
      response => {
        agentprofileData => this.agentprofile = agentprofileData;
        (<HTMLInputElement>document.getElementById("username")).textContent = response.userName;
        ManageagentprofileComponent.username = response.userName;
        (<HTMLInputElement>document.getElementById("firstname")).value = response.agentFirstName;
        (<HTMLInputElement>document.getElementById("lastname")).value = response.agentLastName;
        (<HTMLInputElement>document.getElementById("email")).value = response.agentEmail;
        (<HTMLInputElement>document.getElementById("phone")).value = response.agentPhone;
        (<HTMLInputElement>document.getElementById("password")).value = response.password;
        (<HTMLInputElement>document.getElementById("message")).value = response.message;
        },
      err => {
      console.error('Something went wrong in Agent Profile ', err);
      });
  }

  updateProfile(event) {
    event.preventDefault();
    const target = event.target;
    var data = {
      'userId' : window.localStorage.getItem("userId"),
      'companyId':window.localStorage.getItem("companyId"),
      'agentId':ManageagentprofileComponent.agentId,
      'userName':ManageagentprofileComponent.username,
      'agentFirstName' : target.querySelector("#firstname").value,
      'agentLastName' : target.querySelector("#lastname").value,
      'agentEmail' : target.querySelector("#email").value,
      'agentPhone' : target.querySelector("#phone").value,
      'message' : target.querySelector("#message").value,
      'password' : target.querySelector("#password").value
    };

    this.adminService.modifyAgent(data)
    .subscribe(
      response => {
        console.log("addAgentFormComponent----"+response);
        var responseData = JSON.parse(response);
        console.log("addAgentFormComponent--111--"+responseData.result);
          if('success'==responseData.result){
            this.router.navigate(['/admin/agent']);
            //this.responseMessage = "Generated Username = "+responseData.username+" and Password = "+responseData.password+".";
            //this.showSuccessMessage(event);
          }else if('failed'==responseData.result){
            //this.showErrorMessage(event);
        }

        },
      err => {
      console.error('Something went wrong in Adding Agent By Admin ', err);
      //this.showErrorMessage(event);
      });
  };
  togglePassword = function () {
    var x = (<HTMLInputElement>document.getElementById("password"));
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  backbtnClick= function () {
    this.router.navigate(['./admin/agent']);
  };
}
