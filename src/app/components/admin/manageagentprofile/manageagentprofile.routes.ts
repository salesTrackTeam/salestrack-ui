import {ManageagentprofileComponent} from "./manageagentprofile.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const manageagentprofileRoutes=[
    {
        path:'',
        component:ManageagentprofileComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(manageagentprofileRoutes)],
    exports: [RouterModule]
})
export class ManagegentprofileRoutingModule {}