import { Component } from '@angular/core';
import { User } from "../../models/user";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  loginInfo ={
      first_name: window.localStorage.getItem("userFirstName"),
      last_name:window.localStorage.getItem("userLastName"),
      avatar:window.localStorage.getItem("profilePic"),
      title:window.localStorage.getItem("companyName")
  };
}
