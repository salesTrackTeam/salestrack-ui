import {AddagentformComponent} from "./addagentform.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const addagentRoutes=[
    {
        path:'',
        component:AddagentformComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(addagentRoutes)],
    exports: [RouterModule]
})
export class AddagentformRoutingModule {}