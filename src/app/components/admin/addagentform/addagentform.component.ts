import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../../../service/adminService/admin.service';

@Component({
  selector: 'app-addagentform',
  templateUrl: './addagentform.component.html'
})
export class AddagentformComponent implements OnInit {

  responseMessage:string;

  constructor(
    private router: Router,
    private adminService: AdminService
  ) { }

 
  addLead(event) {
    event.preventDefault();
    const target = event.target;


    var data = {
      'userId' : window.localStorage.getItem("userId"),
      'companyId':window.localStorage.getItem("companyId"),
      'agentFirstName' : target.querySelector("#firstname").value,
      'agentLastName' : target.querySelector("#lastname").value,
      'agentEmail' : target.querySelector("#email").value,
      'agentPhone' : target.querySelector("#phone").value,
      'message' : target.querySelector("#message").value
    };

    this.adminService.addAgent(data)
    .subscribe(
      response => {
        console.log("addAgentFormComponent----"+response);
        var responseData = JSON.parse(response);
        console.log("addAgentFormComponent--111--"+responseData.result);
          if('success'==responseData.result){
            this.responseMessage = "Generated Username = "+responseData.username+" and Password = "+responseData.password+".";
            this.showSuccessMessage(event);
          }else if('failed'==responseData.result){
            this.showErrorMessage(event);
        }

        },
      err => {
      console.error('Something went wrong in Adding Agent By Admin ', err);
      this.showErrorMessage(event);
      });
  };

  showSuccessMessage(event){
    const target = event.target;
    target.querySelector("#response_message").style.display = "block";
    target.querySelector("#main_form").style.display = "none";
    target.querySelector("#error_message").style.display = "none";
    }
  showErrorMessage(event){
    const target = event.target;
    target.querySelector("#response_message").style.display = "none";
    target.querySelector("#main_form").style.display = "none";
    target.querySelector("#error_message").style.display = "block";
    }
  addMore(event){
    const target = event.target;
    (<HTMLInputElement>document.getElementById("firstname")).value = "";
    (<HTMLInputElement>document.getElementById("lastname")).value = "";
    (<HTMLInputElement>document.getElementById("email")).value = "";
    (<HTMLInputElement>document.getElementById("phone")).value = "";
    (<HTMLInputElement>document.getElementById("message")).value = "";
    
    window.document.getElementById("response_message").style.display = "none";
    window.document.getElementById("main_form").style.display = "block";
    window.document.getElementById("error_message").style.display = "none";
    }

  addbtnClick= function () {
    this.router.navigate(['/info','agentlist']);
  };
  backbtnClick= function () {
    this.router.navigate(['./admin/agent']);
  };
  
  ngOnInit() {
}
}
