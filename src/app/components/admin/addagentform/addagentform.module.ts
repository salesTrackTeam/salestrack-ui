import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {AddagentformComponent} from "./addagentform.component";
import {addagentRoutes} from "./addagentform.routes";
import {AdminService} from '../../../service/adminService/admin.service';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      RouterModule.forChild(addagentRoutes)
  ],
  declarations: [
    AddagentformComponent
  ],
  providers: [
    AdminService
]
})
export class AddagentformModule {



 }
