import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {Topnavbar} from "./topnavbar/topnavbar.component";
import {Navigation} from "./navigation/navigation.component";
import {AdminComponent } from './admin.component';
import {AdminRoutingModule } from './admin-routing.module';
import { DealerinfoComponent } from './dealerinfo/dealerinfo.component';
import { AgentprofileComponent } from './agentprofile/agentprofile.component';
import { ManageagentprofileComponent } from './manageagentprofile/manageagentprofile.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    HttpModule
  ],declarations: [
    AdminComponent,
    Navigation,
    Topnavbar
  ]
})
export class AdminModule { }
