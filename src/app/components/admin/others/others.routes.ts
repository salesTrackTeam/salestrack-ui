import {OthersComponent} from "./others.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const othersRoutes=[
    {
        path:'',
        component:OthersComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(othersRoutes)],
    exports: [RouterModule]
})
export class OthersRoutingModule {}