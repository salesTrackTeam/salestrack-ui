import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AgentprofileComponent} from './agentprofile.component';
import {InfopageComponent } from '../infopage/infopage.component';
import {MatDialogModule} from '@angular/material/dialog';
import {RouterModule} from "@angular/router";
import {AdminService} from '../../../service/adminService/admin.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {agentprofileRoutes} from './agentprofile.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(agentprofileRoutes)
  ],
  declarations: [
    InfopageComponent,
    AgentprofileComponent
  ],
  providers:[
    AdminService,
    MatDialog
  ],
  entryComponents: [
    InfopageComponent,
    AgentprofileComponent
  ]
})
export class AgentprofileModule { }
