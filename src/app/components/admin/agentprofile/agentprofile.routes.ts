import {AgentprofileComponent} from "./agentprofile.component";
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const agentprofileRoutes=[
    {
        path:'',
        component:AgentprofileComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(agentprofileRoutes)],
    exports: [RouterModule]
})
export class AgentprofileRoutingModule {}