import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
      path: '',
      component: AdminComponent,
      children: [
          {
              path: '',
              redirectTo: 'home'
          },
          {
              path: 'home',
              loadChildren: './home/home.module#HomeModule'
          },
          {
              path: 'others',
              loadChildren: './others/others.module#OthersModule'
          },
          {
              path: 'agent',
              loadChildren: './infopage/infopage.module#InfopageModule'
          },
          {
              path: 'dealer',
              loadChildren: './dealerinfo/dealerinfo.module#DealerinfoModule'
          },
          {
              path: 'addagent',
              loadChildren: './addagentform/addagentform.module#AddagentformModule'
          },
          {
              path: 'agentprofile',
              loadChildren: './agentprofile/agentprofile.module#AgentprofileModule'
          },
          {
              path: 'manageagentprofile',
              loadChildren: './manageagentprofile/manageagentprofile.module#ManageagentprofileModule'
          },
          {
            path: 'dealerDetails',
            loadChildren: '../agent/dealerdetails/dealerdetails.module#DealerdetailsModule'
       },
          
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
