import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoginRoutingModule } from './login.routes';
import { LoginComponent } from './login.component';
import { AuthenticateService } from '../../service/authenticate.service';
import { UserService } from "../../service/user.service";

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent],
    providers: [
        AuthenticateService,
        UserService
    ]
})
export class LoginModule {}
