import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateService } from '../../service/authenticate.service';
import { User } from "../../models/user";
import { UserService } from "../../service/user.service";
declare const $

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private authenticateService:AuthenticateService,
    private userService : UserService
    ) {}
  errormsg:string;
  focused:boolean;
  authUser(event){
    event.preventDefault();
    this.focused = true;
    this.errormsg = "";
    const target = event.target;
    const company = target.querySelector("#Company").value;
    const username = target.querySelector("#Username").value;
    const password = target.querySelector("#Password").value;
    if (username=='' || password== '') {
      target.querySelector("#errorMsg").style.visibility = "visible"; 
      return;
    }
    //target.querySelector("#login__submit").addClass("processing");
    //document.querySelector("#login__submit").addClass("processing");
    //(<HTMLInputElement>document.getElementById("firstname")).addClass("processing");

    this.authenticateService.login(username,password,company)
    .subscribe(
      userInfoData=> {
        if(userInfoData.loginFlag == "2"){
          this.errormsg = "Password in Invalid";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if(userInfoData.loginFlag == "3"){
          this.errormsg = "User is Deactivated";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if(userInfoData.loginFlag == "4"){
          this.errormsg = "Company is Deactivated";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if(userInfoData.loginFlag == "5"){
          this.errormsg = "User or Company is Invalid";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if(userInfoData.loginFlag == ""){
          this.errormsg = "Something Went Wrong";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if(userInfoData.loginFlag == "1"){
              this.userService.setUserData(userInfoData);
              if( window.localStorage.getItem("userType") == "1"){
                this.router.navigate(['./admin']);
              }else if(window.localStorage.getItem("userType") == "2"){
                this.router.navigate(['./agent']);
              }
        }
      },
      err => {
        this.errormsg = 'Server is not ready';
        target.querySelector("#errorMsg").style.visibility = "visible"; 
      console.error('Something went wrong ', err);
      }); 
  }

  hideError(event){
    const target = event.target;
    target.querySelector("#errorMsg").style.visibility = "hidden";
  }
  ngOnInit() {
    $(document).ready(function() {
  
      var animating = false,
          submitPhase1 = 1100,
          submitPhase2 = 400,
          logoutPhase1 = 800,
          $login = $(".login"),
          $app = $(".app"),
          $demo = $(".demo"),
          $demosignup = $(".demosignup");
      function ripple(elem, e) {
        $(".ripple").remove();
        var elTop = elem.offset().top,
            elLeft = elem.offset().left,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var $ripple = $("<div class='ripple'></div>");
        $ripple.css({top: y, left: x});
        elem.append($ripple);
      };
      
      $(document).on("click", ".login__submit", function(e) {
        if($("#Company").val()!="" && $("#Username").val()!="" && $("#Password").val()!=""){
        if (animating) return;
        animating = true;
        var that = this;
        ripple($(that), e);
        $(that).addClass("processing");
        setTimeout(function() {      
          setTimeout(function() {
          }, submitPhase2 - 70);
          setTimeout(function() {
            stopanimation();
          }, submitPhase2);
        }, submitPhase1);
      }
      });

      function stopanimation(){
          animating = false;
            $(".login__submit").removeClass("success processing");
      }

      $(document).on("click", ".login__signup", function(e) {
        if (animating) return;
        animating = true;
        var that = this;
        ripple($(that), e);
        $(that).addClass("processing");
        setTimeout(function() {
          $(that).addClass("success");
          setTimeout(function() {
            $app.show();
            $app.css("top")
            ;
            $app.addClass("active");
            $demo.addClass("demosignup");
          }, submitPhase2 - 70);
          setTimeout(function() {
            $login.hide();
            $login.addClass("inactive");
            $demo.removeClass("demo");
            animating = false;
            $(that).removeClass("success processing");
          }, submitPhase2);
        }, submitPhase1);
      });
      
      $(document).on("click", ".app__logout", function(e) {
        if (animating) return;
        $(".ripple").remove();
        animating = true;
        var that = this;
        $(that).addClass("clicked");
        setTimeout(function() {
          $app.removeClass("active");
          $demo.addClass("demo");
          $login.show();
          $login.css("top");
          $login.removeClass("inactive");
          $demo.removeClass("demosignup");
        }, logoutPhase1 - 120);
        setTimeout(function() {
          $app.hide();
          animating = false;
          $(that).removeClass("clicked");
        }, logoutPhase1);
      });
    });
  }
}
 
