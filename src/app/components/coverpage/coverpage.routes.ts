import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CoverpageComponent } from './coverpage.component';

const routes: Routes = [
    {
        path: '',
        component: CoverpageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoverpageRoutingModule {}
