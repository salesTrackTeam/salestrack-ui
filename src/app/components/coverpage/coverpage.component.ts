import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateService } from '../../service/authenticate.service';
import { User } from "../../models/user";
import { UserService } from "../../service/user.service";
declare const $

@Component({
  selector: 'app-coverpage',
  templateUrl: './coverpage.component.html',
  styleUrls: ['./coverpage.component.css']
})

export class CoverpageComponent implements OnInit {

  constructor(
    private router: Router,
    private authenticateService:AuthenticateService,
    private userService : UserService
    ) {}
    enquirydata = {
      'name' : "",
      'business' : "",
      'phone' : "",
      'email' : ""
    };
    signupdata = {
      'companyName' : "",
      'companyLoginName' : "",
      'phone' : "",
      'email' : "",
      'userFirstName' : "",
      'userLastName' : "",
      'userName' : "",
      'password' : "",
      'registrationType' : "",
      'companyType' : ""
    };
  errormsg:string;
  signuperrormsg:string;
  focused:boolean;
  authUser(event){
    event.preventDefault();
    this.focused = true;
    const target = event.target;
    this.enquirydata.name = target.querySelector("#name").value,
    this.enquirydata.business = target.querySelector("#business").value,
    this.enquirydata.phone = target.querySelector("#phone").value;
    this.enquirydata.email = target.querySelector("#email").value;
    
    this.authenticateService.enquiry(this.enquirydata)
    .subscribe(
      response => {
        var responseData = JSON.parse(response);
        if('success'==responseData.result){
          this.errormsg = "Details Added Successfully. We will get you back.";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
        if('failed'==responseData.result){
          this.errormsg = "Try Again";
          target.querySelector("#errorMsg").style.visibility = "visible"; 
        }
      },
      err => {
        this.errormsg = 'Server is not ready';
        target.querySelector("#errorMsg").style.visibility = "visible"; 
        console.error('Something went wrong ', err);
      }); 
  }
  signup(event){
    event.preventDefault();
    this.focused = true;
    const target = event.target;
    this.signupdata.companyName = target.querySelector("#cname").value;
    this.signupdata.companyLoginName = target.querySelector("#cid").value;
    this.signupdata.phone = target.querySelector("#uphone").value;
    this.signupdata.email = target.querySelector("#uemail").value;
    this.signupdata.userFirstName = target.querySelector("#fname").value;
    this.signupdata.userLastName = target.querySelector("#lname").value;
    this.signupdata.userName = target.querySelector("#uname").value;
    this.signupdata.password = target.querySelector("#password").value;
    this.signupdata.registrationType = 'trial';
    this.signupdata.companyType = 'Marketing';

    if(this.signupdata.password != target.querySelector("#cpassword").value)
    {
        this.signuperrormsg = "Password Mismatch!";
        target.querySelector("#signuperrormsg").style.visibility = "visible"; 
        return false;
    }
    
    this.authenticateService.signup(this.signupdata)
    .subscribe(
      response => {
        var responseData = JSON.parse(response);
        if('success'==responseData.result){
          this.signuperrormsg = "Registered Successfully. Please wait while login..";
          target.querySelector("#signuperrormsg").style.visibility = "visible"; 
          this.login(this.signupdata.companyLoginName,this.signupdata.userName,this.signupdata.password,target)
        }
        if('companyLoginNameExists'==responseData.result){
          this.signuperrormsg = "Company Id Already taken";
          target.querySelector("#signuperrormsg").style.visibility = "visible"; 
        }
      },
      err => {
        this.signuperrormsg = 'Server is not ready';
        target.querySelector("#signuperrormsg").style.visibility = "visible"; 
        console.error('Something went wrong ', err);
      }); 
  }
  hideError(event){
    const target = event.target;
    target.querySelector("#errorMsg").style.visibility = "hidden";
  }
  login(company,username,password,target){
    this.authenticateService.login(username,password,company)
    .subscribe(
      userInfoData=> {
        this.userService.setUserData(userInfoData);
        this.router.navigate(['./admin']);
      },
      err => {
        this.errormsg = 'Server is not ready';
        target.querySelector("#signuperrormsg").style.visibility = "visible"; 
      console.error('Something went wrong ', err);
      }); 
  }
  ngOnInit() {
    $(document).ready(function() {

      $("a").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();

          var hash = this.hash;
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
       
           window.location.hash = hash;
          });
        } 
      });
    });
  }
}
 
