import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CoverpageRoutingModule } from './coverpage.routes';
import { CoverpageComponent } from './coverpage.component';
import { AuthenticateService } from '../../service/authenticate.service';
import { UserService } from "../../service/user.service";

@NgModule({
    imports: [
        CommonModule,
        CoverpageRoutingModule
    ],
    declarations: [CoverpageComponent],
    providers: [
        AuthenticateService,
        UserService
    ]
})
export class CoverpageModule {}
