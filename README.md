# SalesTrack

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

aws Route 53 value:
-----------------------------
Value-thesalespower.com.
	
Value-NS
	
Value-
ns-1004.awsdns-61.net.
ns-34.awsdns-04.com.
ns-1082.awsdns-07.org.
ns-1565.awsdns-03.co.uk.
ttl-172800
-----------------------------